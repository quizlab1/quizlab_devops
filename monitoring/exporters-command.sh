COMMAND=$1

docker-compose --project-directory  ./ \
	-f exporters/docker/docker-compose.yaml \
	-f exporters/nginx/docker-compose.yaml \
	-f exporters/node/docker-compose.yaml \
	-f exporters/redis/docker-compose.yaml $COMMAND
