local json = require "json"

function cb_print(tag, timestamp, record)
    new_record = record
    noterr, log_obj = pcall(function() return json.decode(record["log"]) end)

    if noterr then
	new_record["level"] = log_obj.level
	new_record["msg"] = log_obj.msg
    	return 1, timestamp, new_record
    else
	new_record["level"] = "UNDEFINED"
	new_record["msg"] = record["log"]
	return 1, timestamp, new_record
    end
end

