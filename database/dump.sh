CONTAINER_NAME=$1

NOW="$(date +"%d-%m-%Y_%s")"
docker container exec -e PGPASSWORD=12345 -it $CONTAINER_NAME pg_dump -d quizlabdb -U quizlab > dump_${NOW}.sql

