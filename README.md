# Quizlab DevOps Documentation

Repository that holds the components of Quizlab Infrastructure.

## Recommendations

- All changes should be made through code and pushed to the Gitlab's repository. For example, when launching of new service, the service should be added to `docker-compose.yml`, where configurations for all services are located.

- Infrastructure should be organized in a way, so that it can be deleted and recreated at any time.

- If one of the components faces downtime or there is a crash in the system, then **postmortem** is written down that has the following fields:

    1. description of the glitch

    2. occurence time

    3. duration of downtime

    4. reason of the glitch

    5. solution (what should be done to prevent that problem from occurring again)

- In critical situations, don't panic. Your desicions and the next steps should be deliberate and balanced.

- If someone noticed a crash in the system, he/she needs to notify others. Other members of the team must reach out the person, who can fix the crash in the system.

- Database backups should be checked out regularly. Even if backup exists, there is no garranty that it would work properly.

- Makefile and CI/CD pipeline description (.gitlab-ci.yml) should be the same in all services of the infrastructure. In future, it is possible to switch to **multi-project pipelines**.

- Before each change of the system, which might touch the stability of the system, the teammates should be notified. The followings are the examples of such cases: adding new service, restarting all services, updating configuration of the server in production, manipulation with the database and so on.
For example, you are adding a new service. You need to tell about it to other people in your team individually or via messenger.

- Rely on common sense in any situation.

## Description

- `database` - contains files for building a test database and scripts for working with the database

- `logging` - centralized collection of logs. Files to run elastic **search**, **kibana**, **fluentbit**

- `monitoring` - system monitoring. **Prometheus**, **prometheus-exporters**, **grafana**, **alertmanager**

- `scripts` - handy scripts.

- `docker-compose.yml` - contains a description for all services: ports, aliases, necessary folders

- `Makefile` - contains basic commands for starting a docker-swarm cluster with specific services

    * **prod-up-swarm** - launches services for production environment, takes configuration from .env file

    * **test-up-swarm** - launches services for test and local development, the configuration is taken from the .env file. The difference from the previous command is that the database also launches along with other services

    * **services** - list of running quizlab services

    * **remove-stack** - remove all services. Do not run this command on the test and production servers under any circumstances

## How to get dump for local development

- Connect to test server via ssh.

- Go to the directory /var/www/quizlab_devops/database.

- Find out the name of the container in which the database is running with the command `docker container list`. The container name starts with 'quizlab_database.1.'. For example: quizlab_database.1cyuz357yzk9uy53zrgco3o19m. Copy the name of the container.

- Run the dump.sh script passing the name of the database container as an argument. (`./dump.sh quizlab_datase.1.cyuz357yzk9uy53zrgco3o19m`).

- Upon completion, a dump file will appear in the current directory: dump_{current-time}.sql.

- To copy to a local machine, you can use **scp**: `scp -i .ssh/QuizLab.pem ubuntu@35.153.66.55/:/var/www/quizlab_devops/database/dump_{current-time}.sql ./` (Please, check whether IP address is the same as specified here).

If the dump file already exists and it comes up in time, you can skip all steps except the last one. Just copy the dump file.

## How to bring up all services locally

To run, you need docker older than version 19.03 and take a dump of the database

- Clone quizlab_devops repository.

- Create .env file `cp env.sample .env`.

- Edit .env file. If you need to start services that are currently running in production, you need to change the value of service_name_tag to `production`. For example: user_service_tag = production. If you need to run take docker images from the test server: service_name_tag = latest.

- Start services with the command `make test-swarm-up`

- Find out the name of the database container.

- Restore dump. `cat dump_file.sql | docker container exec -i {db_container_name} psql -d quizlabdb -U quizlab.

- Run the command `make services`. The REPLICAS column in all services should be equal to 1/1.

## Working with config files

If you need to add an environment variable for the service, you need to do it through code

- Modify docker-compose.yml first. And send changes to master.

- Next, download the changes to the server `git pull origin master`.

- Edit the .env file if needed.

- Make sure in .env file service_name_tag=latest for test environment , and service_name_tag=production in production.

- If on the test server - run `make test-up-swarm`, on production - `make prod-up-swarm`.

## Service Discovery

Docker Swarm represents a service discovery, which means that you can contact one service by another with alias. For example: you cam make a request from user_service to email_service by making a request to email_service:port. Therefore, in order not to add host and port to environment variables every time,
alias and port must be specified as the default value.

Service aliases:

- user_service:9000
- email_service:9001
- redis:6379

## Adding a new service

- Add Makefile. Makefile should contain commands `make build-image`,`make push-image`. Service name and registry address must be in the .build_info file. Variable APP = service_name, REGISTRY = registry.
Makefile can be copied from another service.

- Configure CI / CD for the service. If the service is written in Golang, copy .gitlab-ci.yml from another service.

- Add service description to docker-compose.yml. Submit to repository.

- Build a service for staging. Submit to repository. {registry}/{quizlab_service}:latest.

- Connect to the test server via ssh. Download changes `git pull origin master`.

- If needed, add service parameters (configs) to the .env file.

- Start the service with the command `make test-up-swarm`.

- After launching a new service you need to check if everything is fine in the system. Either run the tests, or ask the tester to help to check the staging.

If everything is fine, you can proceed to the production launch.

- Build a docker image for production. Must have a production tag.

- Download changes in production environment.

- Add service_tag to .env file: service_name_tag = production.

- Start the service with the command `make prod-up-swarm`.

- Conduct tests, together with testers.
